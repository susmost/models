from susmost import load_lattice_task, mc
import numpy as np
import os
from mpi4py import MPI
from ase import units
from ase.parallel import parprint

lt = load_lattice_task('model')
parprint (lt)
lt.set_property('coverage', {f'nb2coh2':1.0} )
kB = units.kB * 1.0 	# eV/K

Ts = [25., 50., 75., 100., 125., 150., 175., 200., 250., 300., 350., 400., 450., 500.]
Ts = [T*2.0 for T in Ts]
L = 60


for mu0 in np.arange(-2100.0, 700.0, 50)/1000.0:
    lt.set_ads_energy(f'nb2coh2', mu0)
    m = mc.make_metropolis(lt, L, Ts, kB)
    mc.run(m, log_periods_cnt=5, log_period_steps = 10000*m.cells_count, relaxation_steps = 100000*m.cells_count, traj_fns=[f'traj_{T}_{mu0}.xyz' for T in Ts])
    d = mc.stat_digest(m, verbose=False)	# calc summary on samples statistics    
    if MPI.COMM_WORLD.Get_rank() == 0:
        for T,di in zip(Ts,d):
            cov = di["coverage"]
            E = di["Energy"]
            print(f'STAT: {mu0} {T} {cov["mean"]:.3f} {cov.get("neff_int", 0):.3f} {cov["std"]:.3f} E = {E["mean"]:.3f} E_D = {E["std"]:.3f}')
