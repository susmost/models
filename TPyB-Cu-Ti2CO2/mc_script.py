#!python -u

# Here is the simulation example of the self-assembly of 2D metal-organic networks comprising 1,3,5-tris (pyridyl)benzene (TPyB) molecules and copper atoms
# on the oxygen-terminated titanium carbide MXene surface. The lattice model of the TPyB-Cu networks on the energetically heterogeneous Ti2CO2(0001) surface
# is based on DFT calculations of the structure and energy of key adsorption complexes and metal-organic structures.
# This code calculates adsorption isotherms, structure, potential energy, and heat capacity of the adlayer, using the grand canonical Monte Carlo method.

from susmost import load_lattice_task
from susmost import *
from susmost.mc import *
from susmost import mc
import numpy as np
from mpi4py import MPI

lt = load_lattice_task('./tpyb_cu_model')

muTPyB_0 = -110. # Average adsorption energy of TPyB
muCu_0 = -307. # Average adsorption energy of TPyB on strong sites (fcc, hcp, bridge)
delta = 38. # Adsorption energy of Cu on top site is (average + delta)
#muTPyB = 15. # Pressure impact into chemical potential of TPyB in the adlayer
#muCu = 35. # Pressure impact into chemical potential of Cu in the adlayer

lt.set_property('coverage', {'up_tpb_top':19, 'down_tpb_top':19, 'down_tpb_fcc':19, 'up_tpb_fcc':19, 'down_tpb_hcp':19, 'up_tpb_hcp':19, 'cu_top':1, 'cu_fcc':1, 'cu_hcp':1})
lt.set_property('TPyB_cnt', {'up_tpb_top':1, 'down_tpb_top':1, 'down_tpb_fcc':1, 'up_tpb_fcc':1, 'down_tpb_hcp':1, 'up_tpb_hcp':1})
lt.set_property('Cu_cnt', {'cu_top':1, 'cu_fcc':1, 'cu_hcp':1})

print (lt.states)

T_list = [500, 1000, 1500, 2000, 2500, 3000, 4000] 	# 7 cpu

if MPI.COMM_WORLD.Get_rank() == 0:
		f = open('results.dat','w')

L = 32		# lattice size in surface unit cells 3x3. Actual size is 32*3 = 96
f = open('results.dat','w')
f.write('T	muTPyB	muCu	coverage	TPyB	Cu	CpU	CpH	Qd	zero_time	time_exp	ac_time_int	total_lenght	H	U''\n')

muTPyB = -250
delta_mu = 20.0
for muCu in np.arange(-440.0, -320.0+0.0001, delta_mu):

	lt.set_ads_energy('cu_top', muCu_0 + delta - muCu)	# kJ/mol
	lt.set_ads_energy('cu_fcc', muCu_0 - muCu)	# kJ/mol
	lt.set_ads_energy('cu_hcp', muCu_0 - muCu)	# kJ/mol
	lt.set_ads_energy('down_tpb_top', muTPyB_0 - muTPyB)	# kJ/mol
	lt.set_ads_energy('up_tpb_top', muTPyB_0 - muTPyB)	# kJ/mol
	lt.set_ads_energy('down_tpb_fcc', muTPyB_0 - muTPyB)	# kJ/mol
	lt.set_ads_energy('up_tpb_fcc', muTPyB_0 - muTPyB)	# kJ/mol
	lt.set_ads_energy('down_tpb_hcp', muTPyB_0 - muTPyB)	# kJ/mol
	lt.set_ads_energy('up_tpb_hcp', muTPyB_0 - muTPyB)	# kJ/mol
	m = make_metropolis(lt, L, T_list, 0.00831)
	m.phys_diffusion = True
	m.diff_cui_min = 1
	m.diff_cui_max = 4
	m.min_phys_diff_edge_idx = 1
	m.max_phys_diff_edge_idx = 120

	l = run(m, log_periods_cnt=80, log_period_steps = 125*m.cells_count, relaxation_steps = 15000*m.cells_count, traj_fns=["muTPyB={}_muCu={}_T={}.xyz".format(muTPyB, muCu, T) for T in T_list])
	stat_digest(m) # print mc.run() statistics

	if MPI.COMM_WORLD.Get_rank() == 0:
		for T, param_log, cov, TPyB, Cu in zip(T_list, m.full_params_log, m.means('coverage'), m.means('TPyB_cnt'), m.means('Cu_cnt')):
			print (T, cov, TPyB, Cu)
			stat = np.mean(param_log,axis=0)[[-2, -3]] # H, U
			stdev = np.std(param_log[:, -2])
			if stdev > 1e-6:
				acf = mc.autocorr_func(param_log[:, -2], method="fft")
				acf_a = mc.autocorr_analysis(acf)
				acf_log = ('{}\t'*4).format(acf_a.zero_time, acf_a.ac_time_exp, acf_a.ac_time_int, len(acf_a.acf))
			else:
				acf_log = "NaN"
			E_2 = 0
			H_2 = 0
			covE = 0
			cov_2 = 0
			for log_row in param_log:
				E_2 += log_row[-3]**2.0
				H_2 += log_row[-2]**2.0
				covE += log_row[2]*log_row[-3]
				cov_2 += log_row[2]**2.0
			E_2 /= len(param_log)
			covE /= len(param_log)
			cov_2 /= len(param_log)
			Qd = -(covE - cov*stat[1])/(cov_2-cov**2.0)
			CpU = (L^2)*(E_2 - stat[1]*stat[1])/(T*T*0.00831)
			CpH = (L^2)*(H_2 - stat[0]*stat[0])/(T*T*0.00831)
			s = ('{}\t'*12).format(T, muTPyB, muCu, cov, TPyB, Cu, CpU, CpH, Qd, acf_log, *stat)
			f.write( s + '\n')
			f.flush()

f.close()
