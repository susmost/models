"""
	Interactions model is described in https://doi.org/10.1063/1.5122041
	E(r) == 14.421 * (0.4546 ^ 2) * (1/r^3 - 2.9603 / r^4 + 43.5267 / r ^ 5)
"""

from susmost import load_lattice_task, make_tensor, \
	solve_TRG, joined_cells_lattice_task
import numpy as np

lt = load_lattice_task('./')
lt = joined_cells_lattice_task(lt, (2,2))
T = 300. 		# K
kB = 0.000086 	# eV/K
beta = 1./(kB*T) 	# inverse temperature 1/(kB*T)


def omega(lt, mu, beta):
	lt.set_ads_energy('NH3-atop-V3C2', mu)
	W = make_tensor(lt, beta)
	lnZ = solve_TRG(W, 32)
	return -lnZ/beta

def coverage(lt, mu, beta):
	dmu = 0.01
	omega1 = omega(lt, mu, beta)
	omega2 = omega(lt, mu+dmu, beta)
	return (omega2 - omega1) / dmu
	
for mu in np.linspace(-0.3, 0.3, 11):
	print (mu, coverage(lt, mu, beta))
