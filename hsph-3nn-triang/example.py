from susmost import load_lattice_task, joined_cells_lattice_task, make_tensor, solve_TRG

lt = load_lattice_task('./')
M = 2
lt = joined_cells_lattice_task(lt, M) # join lattice sites to leave only interactions between neighbours

lt.set_ads_energy('atop-triang', 1.)

beta = 1. 		# inverse temperature 1/(kB*T)

W = make_tensor(lt, beta)

lnZ = solve_TRG(W, max_count=32) / (M**2) # natural logarithm of partition function per lattice site
freeE = -lnZ/beta

print ("Free energy per lattice site = ", freeE)



