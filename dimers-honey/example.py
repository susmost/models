from susmost import load_lattice_task, joined_cells_lattice_task, make_tensor, solve_TM, transferm
import numpy as np
import os

path = os.path.dirname(__file__)

lt = load_lattice_task(path)

h = 20.0 #kJ/mol

N = 5 # N = M/2 to compare with https://doi.org/10.1103/PhysRevB.97.085408

lt.set_property('coverage', {"atop-honey":0.5, "dimer-honey":1.0})
lt.set_property('atop_count', {"atop-honey":1.0, "dimer-honey":0.0})
lt.set_property('dimer_count', {"atop-honey":0.0, "dimer-honey":1.0})
lt.set_property('density', {"atop-honey":0.5, "dimer-honey":.5})

for mu in np.arange(-30., 33.+1, 2.):

	lt.set_ads_energy('atop-honey', -mu)
	lt.set_ads_energy('dimer-honey', -(mu + h))

	kB = 0.0083 	# kJ/(mol*K)
	T = 200 # K
	beta = 1./(kB*T) 		# inverse temperature 1/(kB*T)

	W = make_tensor(lt, beta, scheme='TM')
	tm_sol = solve_TM(W,N)
	
	cov = np.sum(tm_sol.probs * transferm.average_props(lt.states, N, 'coverage', beta))
	atop_count = np.sum(tm_sol.probs * transferm.average_props(lt.states, N, 'atop_count', beta))
	dimer_count = np.sum(tm_sol.probs * transferm.average_props(lt.states, N, 'dimer_count', beta))
	dens = np.sum(tm_sol.probs * transferm.average_props(lt.states, N, 'density', beta))
	
	A = -np.log(tm_sol.lr)/beta
	
	print ("tm_sol", mu, A, cov, atop_count, dimer_count, dens, tm_sol.lr, tm_sol.ll)



