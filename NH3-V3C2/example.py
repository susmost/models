"""
	Interactions model is described in https://doi.org/10.1063/1.5122041
	E(r) == 14.421 * (0.4546 ^ 2) * (1/r^3 - 2.9603 / r^4 + 43.5267 / r ^ 5)
"""

from susmost import load_lattice_task, make_tensor, \
	solve_TRG, joined_cells_lattice_task

lt = load_lattice_task('./')
lt = joined_cells_lattice_task(lt, (2,2))
lt.set_ads_energy('NH3-atop-V3C2', -0.1)
T = 300. 		# K
kB = 0.000086 	# eV/K
beta = 1./(kB*T) 	# inverse temperature 1/(kB*T)

W = make_tensor(lt, beta)
lnZ = solve_TRG(W, 32)
print (lnZ)
