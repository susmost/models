# Models

Adsorption models for SuSMoST ( https://susmost.com )

For visualization use 3Dmol.js. For example:

```
https://3dmol.org/viewer.html?url=https://gitlab.com/susmost/models/-/raw/main/TPA-Fe-Cu100/sample_025.xyz?ref_type=heads&select=all&style=sphere
```

https://3dmol.org/viewer.html?url=https://gitlab.com/susmost/models/-/raw/main/TPA-Fe-Cu100/sample_025.xyz?ref_type=heads&select=all&style=sphere
