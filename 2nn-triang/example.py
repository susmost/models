from susmost import load_lattice_task, make_tensor, \
	solve_TRG, joined_cells_lattice_task
from susmost.mc import Lattice
import os
import numpy as np

def test_join(lt, S):
	S = np.array(S)
	print (f"S ==\n{S}\ndet(S) ==  {np.linalg.det(S):.3}")
	lt1 = joined_cells_lattice_task(lt, S)
	#print (lt1.edges_dict)
	print (f"States ({lt1.states_count}):",[s for s in lt1.states])
	return lt1


path = os.path.dirname(__file__)
print (path)
lt0 = load_lattice_task(path)
lt0.set_ads_energy('atop-triang', -0.2)
T = 300. 		# K
kB = 0.000086 	# eV/K
beta = 1./(kB*T) 	# inverse temperature 1/(kB*T)
W = make_tensor(lt0, beta)
print ("^^^^^^^ Too small unit cell - missed long-range interaction")
print ("vvvvvvv Lattice tasks with joined cells ")

print ("Naive 2x2 join:")
test_join(lt0, [[2,0,0], [0,2,0], [0,0,1]])

print ("Join 3 cells in a 3-mer:")
test_join(lt0, [[1,1,0],[-1,2,0],[0,0,1]])


print ("Join 3 cells in a triangle:")
lt = test_join(lt0, [[1,1,0],[-2,1,0],[0,0,1]])


W = make_tensor(lt, beta)
lnZ = solve_TRG(W, 36)
print ("Free E =",-lnZ/beta)

print ("Join 2 cells in orthogonal cell:")
lt_orth = test_join(lt0, [[1,0,0],[-1,2,0],[0,0,1]])
print(lt_orth.unit_cell)
lat = Lattice(lt_orth, [17,10])	# approximately square lattice_configs
lat.save_as_xyz("lt_orth.xyz")
