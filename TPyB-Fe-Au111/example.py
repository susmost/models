#!python -u
from susmost import *
import numpy as np
from time import time
	
lt = load_lattice_task(".")

kB = 0.00831

print (lt)
lt = joined_cells_lattice_task(lt, 2)
print (lt)

def calc_lnZ(T, muTPB, muCu):
	lt.set_ads_energy('tpb', muTPB)
	lt.set_ads_energy('cu', muCu)
	beta = 1./(kB*T)
	W = make_tensor(lt, beta, scheme='generic')
	lnZ = solve_TRG(W, max_count = 40, max_value = 0, atol_lnZ = 1e-9, contractions_count = 30,
					 tn4x3thrs=1500, contraction_block_width=100, verbose=True)
	return lnZ

def calc_E(T, muTPB, muCu):
	lnZ = calc_lnZ(T, muTPB, muCu)
	beta = 1./(kB*T)
	return -lnZ/beta

def calc_theta_TPB(T, muTPB, muCu):
	E1 = calc_E(T, muTPB, muCu)
	E2 = calc_E(T, muTPB-dmu, muCu)
	return (E2-E1)/dmu

dmu = 0.01
T = 200.
muCu = 19.

for muTPB in np.linspace(16.00, 32.00, 32):
	t0 = time()
	E = calc_E(T, muTPB, muCu)
	print ("E=",muTPB, muCu, E, time()-t0)

