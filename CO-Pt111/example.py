from susmost import load_lattice_task
from susmost import mc

lt = load_lattice_task('./')

lt.set_ads_energy('ontop', -0.015)	# eV
L = 30			# lattice size
Ts = [2.0, 8.119878894794397, 10.375217380314396, 12.0] # K
kB = 0.000086 	# eV/K
m = mc.make_metropolis(lt, L, Ts, kB)
mc.run(m, 10, m.cells_count*1000) # 10k MCS steps
mc.stat_digest(m) # print mc.run() statistics

