"""
	Interactions fitting procedure is described in AIP Conference Proceedings of
	OGE Conference 2020 by T.R. Kaumova and P.V. Stishenko
"""

from susmost import load_lattice_task
from susmost import mc
import numpy as np
from ase.parallel import paropen

lt = load_lattice_task('./')
kB = 0.000086 	# eV/K
Ts = np.geomspace(300, 2900, 8)
with paropen('results.txt','w', buffering=1) as f:
	f.write(f'T(K):\t{("{: 6.0f} "*len(Ts)).format(*Ts)}\n')
	for mu in [-1., -0.05, 0.13, 0.28, 1.0]:
		lt.set_ads_energy('O', mu)
		m = mc.make_metropolis(lt, 36, Ts, kB)
		mc.run(m, 10, 1000000, params_period_steps=100) # 10x1000000 steps with each 100-th step sampling
		f.write(f'{mu:4.2f}:\t{("{: 6.2f} "*len(Ts)).format(*m.means("coverage"))}\n')

