from susmost import load_lattice_task
from susmost import mc

lt = load_lattice_task('./')

# Conditions for the single-row phase (C): muTPA = 25, muFe = 25, L = 24
# Conditions for the ladder phase (A): muTPA = 15, muFe = 35, L = 16
# Fadeeva A.I. et al. // J. Phys. Chem. C. 2019. Vol. 123, # 28. P. 17265.

muTPA = 15.
muFe = 35.
lt.set_ads_energy('TPA', muTPA)	# kJ/mol
lt.set_ads_energy('Fe', muFe)	# kJ/mol

L = 16			# lattice size
T = [400, 500, 700, 1000] 		# K
kB = 0.0083 	# kJ/(mol*K)
m = mc.make_metropolis(lt, L, T, kB)
mc.run(m, log_periods_cnt=20, log_period_steps = 1000*m.cells_count, \
	relaxation_steps = 500000*m.cells_count)
mc.stat_digest(m) # print mc.run() statistics
